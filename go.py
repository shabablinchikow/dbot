#!/usr/bin/python3.6
import asyncio
import os
import sys
from pathlib import Path

import discord
import basebot

from constants import *
from specialmessages import FileToSend
from time import time
from helpers import gen_text_model

# create client and bot
client = discord.Client()
bot = basebot.BaseBot(client)

# create channel object
ch_log = discord.Object(id=LOG_ID)
ch_all = discord.Object(id=MAIN_CHANNEL_ID)
ch_test = discord.Object(id=TEST_ID)

# generate dictonary for markovify
gen_text_model()


def reload_module(name):
    if name in sys.modules:
        del sys.modules[name]
    __import__(name)


def reload_modules():
    bot.reactions = []
    bot.reaction("")(reload_action)

    # always include root modules directory
    MODULE_GROUPS.append("")

    for module_group in MODULE_GROUPS:
        sys.path.insert(0, MODULES_DIR + module_group)
        for m in os.listdir(MODULES_DIR + module_group):
            if m.endswith(".py"):
                name = os.path.splitext(m)[0]
                bot.log(f"Reloading module '{name}'")
                reload_module(name)
        bot.lastreload = time()


def reload_action(mes, res, frm, ch):
    if os.path.getmtime(os.path.join(MODULES_DIR)) > bot.lastreload:
        reload_modules()


bot.lastreload = time()
reload_modules()


@client.event
@asyncio.coroutine
def on_ready():
    print('Connected!')
    print('Username: ' + client.user.name)
    print('ID: ' + client.user.id)


@client.event
@asyncio.coroutine
def on_message(message):
    # ignore self
    #if message.author == client.user:
    #    return
    # ignore any other bots
    if any(message.author == name for name in OTHER_BOTS):
        return

    try:
        if BOTS_ROLE in message.author.roles:
            return
    except AttributeError:
        pass


    # restrict to test channel if testing
    if TESTING and message.channel.id != ch_test.id:
        return

    # ignore empty messages
    if not message.content:
        return

    # check if there is an ongoing dialog
    user_id = message.author.id
    for dia_id, dia in bot.dialogs.items():
        if dia_id == user_id:
            print("stage: ", dia.stage, ", end:", dia.dead, "end message: ", dia.end_message)
            if dia.dead:
                end_message = dia.end_message
                bot.dialogs.pop(user_id)
                yield from client.send_message(message.channel, end_message)
                return
            answer = dia.process(message.content)
            if message:
                yield from client.send_message(message.channel, answer)
                return
    else:
        # get bot's reaction
        msg = bot.onmessage(message)

    # if we got file, send it
    if isinstance(msg, FileToSend):
        my_file = Path(msg.get_file_path())
        if my_file.is_file():
            yield from client.send_file(message.channel, msg.get_file_path())
            return
        else:
            yield from client.send_message(message.channel, f"Я не нашёл файла {msg.get_file_path()}.")
            return
    if not msg:
        return
    if len(msg) > 2000:
        yield from client.send_message(message.channel, msg[0:1999])
        return
    if TESTING:
        yield from client.send_message(message.channel, "test: " + msg)
        return
    yield from client.send_message(message.channel, msg)
    return


@client.event
@asyncio.coroutine
def on_member_join(member):
    mem = member.name
    if member.nick:
        mem = member.nick
    msg = f"{mem} вошёл в комнату."
    msg_welcome = f"Добро пожаловать, {member.mention}!"
    yield from client.send_message(ch_all, msg + '\n' + msg_welcome)
    return


@client.event
@asyncio.coroutine
def on_member_remove(member):
    mem = member.name
    if member.nick:
        mem = member.nick
    msg = f"{mem} вышел из комнаты."
    yield from client.send_message(ch_all, msg)
    return


@client.event
@asyncio.coroutine
def on_member_ban(member):
    mem = member.name
    if member.nick:
        mem = member.nick
    msg = f"{mem} был забанен."
    yield from client.send_message(ch_all, msg)
    return


@client.event
@asyncio.coroutine
def on_member_unban(user):
    mem = user.name
    if mem.nick:
        mem = mem.nick
    msg = f"{mem} был разбанен."
    yield from client.send_message(ch_all, msg)
    return


@client.event
@asyncio.coroutine
def on_member_update(before, after):
    if before.nick != after.nick:
        aft = ""
        bef = ""
        if before.nick:
            bef = before.nick
        else:
            bef = before.name
        if after.nick:
            aft = after.nick
        else:
            aft = after.name
        yield from client.send_message(ch_all, f"**{bef}** теперь известен как **{aft}**")
        return


client.run(TOKEN)
client.close()
