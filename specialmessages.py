
class BreakingMessage(str):
    pass


class LowPriorityMessage(str):
    pass


class FileToSend:
    def __init__(self, path):
        self.path = path

    def get_file_path(self):
        return self.path
