# По ключевым словам "афоризм" и "почему" бот ответит умным афоризмом

from random import choice, randint

from go import bot
from helpers import make_sent
from quotes import APHORISMS
from specialmessages import LowPriorityMessage


@bot.reaction("афоризм", personal=True)
def reaction(mes, res, frm, ch):
    return LowPriorityMessage(choice(APHORISMS))


@bot.reaction("почему", personal=True)
def reaction(mes, res, frm, ch):
    r = randint(0, 5)
    if r < 2:
        return "Потому что " + make_sent().lower()
    else:
        return LowPriorityMessage(choice(APHORISMS))
