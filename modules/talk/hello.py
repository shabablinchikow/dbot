from go import bot
from specialmessages import LowPriorityMessage
from helpers import upperfirst


@bot.reaction("(привет|здравствуй|зд[ао]рово|здравствуйте).*", personal=True)
def reaction(mes, res, frm, ch):
    return f"{upperfirst(res.group(1))}, {frm}!"


@bot.reaction(r"^.{0,6}(всем )\b(привет|зд[оа]рово|ку|кусь|hi|споки|бб|пока|хай|куськи|кукусь|кукуськи|снов|\
утра|доброе утро|добрый день|добрый вечер|доброй ночи|спокойной ночи|доброго утра|доброго утречка|доброе утречко)\b",
personal=False)
def reaction(mes, res, frm, ch):
    return f"{upperfirst(res.group(2))}, {frm}!"


@bot.reaction(r"\b(привет|здорово|ку|кусь|hi|споки|бб|пока|хай|куськи|кукусь|кукуськи|снов|утра|доброе утро|\
добрый день|добрый вечер|доброй ночи|спокойной ночи|доброго утра|доброго утречка|доброе утречко)\b.{0,6}(всем)",
personal=False)
def reaction(mes, res, frm, ch):
    return f"{upperfirst(res.group(1))}, {frm}!"


@bot.reaction("утрачиков", personal=False)
def reaction(mes, res, frm, ch):
    return LowPriorityMessage(f"хуячиков")
