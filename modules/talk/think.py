from go import bot
import markovify
from helpers import load_obj
from random import choice

@bot.reaction("думаешь (о|про|обо) (.*)", personal=True)
def reaction(mes, res, frm, ch):
    theme = res.group(2).strip("!,.?")
    text_model = load_obj("text_model")
    if not text_model:
        return ""
    options = ["А что тут думать...", "Ну как сказать...", "Вот что скажу!", "Тут такое дело...", "Короче."]
    return choice(options) + ' ' + text_model.make_sentence_with_start(theme, strict=False).lower().capitalize()
