# Для реакции на вопрос "кто/кому/кого" выбирает случайного участника сервера, который находится онлайн.

from go import bot
from random import choice
from discord import Status

@bot.reaction(r"\b(кто|кого|кому)\b", personal=True, exclude=r"(кто ты|ты кто|шампанского|шампанское)")
def reaction(mes, res, frm, ch):
    roster = bot.get_roster()
    chosen = ""
    while True:
        member = choice(list(roster))
        if any(member.status == st for st in (Status.idle, Status.online, Status.dnd)):
            break
    if member.nick:
        chosen = member.nick
    else:
        chosen = member.name
    mes = choice(
        ["Кто-кто, {}!", "{} конечно же.", "Это {}, я уверен!", "Хм-м-м... Может {}",
         "Да это же {}!", "Я думаю это {}", "Да это точно {}!"])
    return mes.format(chosen)
