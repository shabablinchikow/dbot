# Аналогично модулю when.py, только для вопроса "где"

import re
from random import randint, choice

from go import bot
from helpers import load_obj, save_obj
from specialmessages import LowPriorityMessage
from wheres import WHERES


@bot.reaction(r"\bгде\b", personal=True)
def reaction(mes, res, frm, ch):
    try:
        wh = re.findall(r"(где .*)", mes)[0]
    except:
        print("Error! Check out say.py")
        return
    parsed = load_obj('where')
    if not parsed:
        parsed = []
    if (len(parsed) > 300):
        del parsed[randint(0, 300)]
    if (len(wh) < 200) and (not wh in parsed) and not "@" in wh:
        parsed.append(wh.strip('?!.'))
    save_obj(parsed, 'where')

    if choice([0, 1, 2, 3]) == 0 or len(parsed) < 5:
        return LowPriorityMessage(choice(WHERES))
    else:
        return LowPriorityMessage("Там, " + choice(parsed))
