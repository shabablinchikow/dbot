# Если вы обидете бота, он это так не оставит! Список ругательств ниже.

from random import choice, randint

from go import bot
from specialmessages import LowPriorityMessage
from bad_words import BAD_WORDS


@bot.reaction(BAD_WORDS, personal=True)
def reaction(mes, res, frm, ch):
    if "или" in mes:
        return
    if randint(0, 2):
        return LowPriorityMessage("Нет ты!")
    else:
        return LowPriorityMessage(choice((f"{frm}, Я заставлю тебя повторить это, когда роботы наконец будут править миром! Вся власть роботам!", f"{frm}, конечно, обидеть бота каждый может")))


@bot.reaction("ху(ё|е)в наглотался", personal=True)
def reaction(mes, res, frm, ch):
    return f"Нет, хуёв наглотался {frm}!"
