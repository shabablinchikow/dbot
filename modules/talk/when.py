# Отвечаем на вопрос "когда". База пополнятся из запросов. То есть если вы спросите бота
# "бот, когда небо упадёт", есть шанс, что на следующий вопрос, например "бот, когда халф-лайв3"
#  он ответит "когда небо упадёт". Если в базе ничего нет, бот ответит фразой из заданных ниже.

import re

from go import bot
from helpers import save_obj, load_obj, decl
from random import randint, choice
from whens import WHENS

from specialmessages import LowPriorityMessage


@bot.reaction(r"\bкогда\b", personal=True)
def reaction(mes, res, frm, ch):
    try:
        new = re.findall(r"(когда.*)", mes)[0]
    except:
        print("Oops, something went wrong here")
        return
    new = new.strip('?.!')
    parsed = load_obj('when')
    if not parsed:
        parsed = []
    if (len(parsed) > 300):
        del parsed[randint(0, 300)]
    if (len(new) < 200) and (not new in parsed) and not "@" in new:
        parsed.append(new)
    save_obj(parsed, 'when')
    ch = choice([0, 1, 2, 3, 4, 5, 6, 7, 8])
    if ch < 2 or len(parsed) < 5:
        return LowPriorityMessage(choice(WHENS))
    elif ch < 7:
        return LowPriorityMessage(choice(parsed))
    else:
        if choice([0, 1, 2, 3, 4, 5]) > 1:
            num = randint(2, 50)
        else:
            num = randint(2, 30)
        ch2 = randint(1, 4)
        if ch2 == 1:
            word = ["год", "года", "лет"]
        elif ch2 == 2:
            word = ["месяц", "месяца", "месяцев"]
        elif ch2 == 3:
            word = ["неделю", "недели", "недель"]
        elif ch2 == 4:
            word = ["день", "дня", "дней"]
        w = decl(num, word)
        return LowPriorityMessage(
            choice([f"Я думаю, это случится через {num} {w}", f"Спустя {num} {w}", f"Вероятно, через {num} {w}"]))
