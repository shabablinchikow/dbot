from go import bot
from specialmessages import LowPriorityMessage

bot.version = "1.0"


@bot.reaction("версия", personal=True)
def reaction(mes, res, frm, ch):
    str = f"Моя версия — {bot.version}"
    return LowPriorityMessage(str)
