from go import bot
from random import randint, choice
from dialog import Dialog
from constants import OWNER_ID
import enum
import re

class GameStage(enum.Enum):
    WAITING = 0
    READY = 1
    PLAYING = 2
    FINISHED = 3

#TODO
class RequestBulletsNumberDialog(Dialog):
    pass

class RouletteGame():

    def __init__(self, players = [], bullets_number = 1, stage=GameStage.READY): 
        self.players = players
        self.dead = []
        self.bullets_number = bullets_number
        self.stage = stage
        self.bullets = [0, 0, 0, 0, 0, 0]
        self.pos = randint(0,5)
        i = 0
        while i != bullets_number: 
#            print('i = ', i)
            pos = randint(0,5)
            if not self.bullets[pos]:
                self.bullets[pos] = 1
                i += 1
        print('RouletteGame[DEBUG]' + f' Создана игра в рулетку, позиция: {self.pos}, патроны: {self.bullets}')


    def shoot(self, player, turned = 0):
        if self.stage == GameStage.READY:
            self.stage = GameStage.PLAYING
        if turned:
            answer = f'{player} крутит барабан и стреляет... '
        else:
            answer = ''
        print(f'RouletteGame[DEBUG]: Позиция = {self.pos}, пули: {self.bullets}')
        if self.players and player not in players:
            return "Ты не играешь! Подожди, пока эти закончат, пистолет-то один..."

        g = "получил"
        if self.bullets[self.pos]:
            self.dead.append(player)
            self.bullets[self.pos] = 0
            self.pos = (self.pos + 1) %6
            end = self.check_for_end()
            if end:
                end = ' ' + end
            if not randint(0,5):
                g = "поймал"
                return answer + f":boom::boom::boom: {player} маслину {g}" + end
            return answer + f":boom::boom::boom: {player} {g} пулю в лоб..." + end
        else:
            self.pos = (self.pos + 1) %6
            end = self.check_for_end()
            return answer + f"Клац! Уф, {player}, повезло тебе!" + end

    def turn(self, player):
        debug = 'RouletteGame[DEBUG]' + f' {player} крутит барабан, позиция меняется с {self.pos} на '
        self.pos = randint(0,5)
        debug += f'{self.pos}, патроны: {self.bullets}.'
        print(debug)
        return self.shoot(player, 1)

    def check_for_end(self):
        if 1 not in self.bullets:
            bot.remove_game(self)
            #start new game with random bullets
            bull = randint(0,5)
            game = RouletteGame(bullets_number=bull)
            bot.add_game(game)
            return f"Конец игры! Патроны закончились.\nЗаряжаю заново, в барабане {bull} патронов."
        return ""


@bot.reaction(r"\b(рулетка|рулеточка|рулетку|рулеточку)", personal=True)
def reaction(mes, res, frm, ch):
    if 'RouletteGame' in bot.games.keys():
        return 'Погоди... Играют вон, не видишь? Пистолет-то один!'
    answer = "Заряжаю пистолет!"
    players = []
#    if not '@' in mes:
#        answer += ' ' + 'Играют все! Можете просто брать пистолет и стрелять.'
#    else:
#        p = re.compile(r'<!?@.*>')
#        players = p.findall(mes)
#        for pl in players:
#            pl = pl.replace('!', '')
#        if frm in players:
#            players.remove(frm)
#        players = set(players)
#        if not players:
#            answer += ' ' + 'Играют все! Можете просто брать пистолет и стрелять.'
#        else:
#            answer += ' ' + 'Играют ' + frm
#            for pl in players:
#                answer += ', ' + pl
#            answer += '!'

    if not any(str(x) in mes for x in range(0,6)):
        bullets = 1
    else:
        mes_to_search = mes
        for pl in players:
            mes_to_search.replace(pl, '')
        p = re.compile(r'[0-6]')
        bullets = p.findall(mes_to_search)[0]
        bullets = int(bullets)
        #TODO dialog to request number of bullets
    if not bullets:
        bullets = 1
    game = RouletteGame(players, bullets)
    bot.add_game(game)
    answer += ' ' + f'Заряжено {bullets} патронов, барабан крутится... Начинаем игру!'
    return answer

@bot.reaction(r"\b(бах|бабах|огонь|пли|стреляй|:tt:|<:tt:337706303419842560>|стреляю|курок|висок|gun|кручу|верчу|барабан)\b", exclude='roulette', ignorecase=True)
def reaction(mes, res, frm, ch):
    game = bot.games['RouletteGame']
    if not game:
        return ''
#    if frm in game.dead:
#        return f"{frm}, ты мёртв, молчи!!!"
    if any(x in mes.lower() for x in ["кручу", "крутит", "верчу", "вертит", "барабан"]):
        return game.turn(frm)
    return game.shoot(frm)
    
@bot.reaction(r'!roulette reset')
def reaction(mes, res, frm, ch):
#    if not OWNER_ID in frm:
#        return 'У тебя нет прав на это. Зови хозяина.'
    game = RouletteGame()
    bot.remove_game(game)
    return 'Рулетка сброшена!'

