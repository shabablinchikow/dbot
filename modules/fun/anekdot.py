# Бот рассказывает анекдот. Ключевое слово "анекдот", можно указать тему: "бот, анекдот про винду"

from random import choice, randint
from urllib.request import Request, urlopen
from xml.dom.minidom import parseString

from go import bot

MAXITEMS = 20
SENTFILE = bot.datadir + "/aneksent"

ANEKDOT_THEMES = {
    "1 апреля": 16,
    "23 февраля": 91,
    "8 марта": 15,
    "14 февраля": 148,
    "святого валентина": 148,
    "день святого валентина": 148,
    "apple": 40,
    "эппл": 40,
    "windows": 131,
    "виндовс": 131,
    "винду": 131,
    "шинду": 131,
    "шиндовс": 131,
    "абрамовича": 123,
    "авиацию": 101,
    "адама": 135,
    "адама и еву": 135,
    "еву": 135,
    "авто": 39,
    "автомобиль": 39,
    "автомобили": 39,
    "аптеку": 134,
    "аптекаря": 134,
    "аптекарей": 134,
    "армянское радио": 138,
    "машину": 39,
    "машины": 39,
    "автобус": 106,
    "армию": 30,
    "бабу-ягу": 83,
    "бабу ягу": 83,
    "бесплатное": 29,
    "березовского": 132,
    "бориса березовского": 132,
    "билла гейтса": 33,
    "войну": 149,
    "гейтса": 33,
    "барака обаму": 143,
    "обаму": 143,
    "блондинок": 23,
    "блондинку": 23,
    "богатырей": 75,
    "брежнева": 80,
    "британских учёных": 137,
    "британских ученых": 137,
    "брюнеток": 24,
    "брюнетку": 24,
    "буратино": 64,
    "валуева": 89,
    "винни-пуха": 78,
    "винни пуха": 78,
    "вконтакте": 17,
    "вовочку": 2,
    "водку": 93,
    "бухло": 93,
    "врачей": 42,
    "врача": 42,
    "гаи": 3,
    "гарри поттера": 35,
    "геев": 32,
    "гея": 32,
    "грипп": 147,
    "дачу": 136,
    "девушек": 60,
    "девушку": 60,
    "деда мороза": 81,
    "демократию": 71,
    "деньги": 28,
    "детей": 27,
    "диету": 95,
    "дональда трампа": 151,
    "трампа": 151,
    "донцову": 97,
    "егэ": 31,
    "животных": 129,
    "жкх": 128,
    "звёзд": 49,
    "золотую рыбку": 72,
    "ивана-царевича": 84,
    "ивана царевича": 84,
    "инструкции": 48,
    "инструкцию": 48,
    "интернет": 37,
    "каренину": 109,
    "карлсона": 68,
    "кино": 117,
    "кинцо": 117,
    "колобка": 65,
    "конец света": 90,
    "коррупцию": 61,
    "космос": 57,
    "кошек": 55,
    "кошку": 55,
    "красную шапочку": 77,
    "кредиты": 127,
    "кредит": 127,
    "кризис": 9,
    "куклачева": 11,
    "лифт": 125,
    "лифты": 125,
    "любовь": 14,
    "мазая": 74,
    "маршрутку": 103,
    "маршрутки": 103,
    "медведева": 19,
    "метро": 110,
    "милицию": 47,
    "полицию": 47,
    "мента": 47,
    "ментов": 47,
    "ммм": 126,
    "мобильный": 82,
    "мобильник": 82,
    "сотовый": 82,
    "москву": 56,
    "мужа и жену": 53,
    "мужчин": 67,
    "мужчину": 67,
    "мультфильмы": 116,
    "мультфильм": 116,
    "муму": 73,
    "навального": 79,
    "налоги": 98,
    "нанотехнологии": 59,
    "наркотики": 41,
    "новогодний": 13,
    "новый год": 13,
    "новых русских": 45,
    "нового русского": 45,
    "жизнь": 38,
    "объявления": 113,
    "объявление": 113,
    "одессу": 145,
    "одессский": 145,
    "огород": 136,
    "онищенко": 139,
    "отдых": 46,
    "охоту": 58,
    "парашют": 102,
    "пасху": 18,
    "перельмана": 70,
    "пенсионера": 146,
    "пенсионерку": 146,
    "пенсионеров": 146,
    "пенсию": 146,
    "погоду": 96,
    "поезд": 108,
    "поезда": 108,
    "пожарного": 43,
    "пожарных": 43,
    "политику": 36,
    "покемонов": 150,
    "покемона": 150,
    "почту": 121,
    "пошлости": 94,
    "пошлость": 94,
    "приметы": 112,
    "примету": 112,
    "программиста": 26,
    "программистов": 26,
    "прохорова": 66,
    "путина": 8,
    "пушкина": 87,
    "рабиновича": 20,
    "работу": 7,
    "рекламу": 111,
    "ржевского": 4,
    "русский язык": 122,
    "рыбалку": 62,
    "санкции": 144,
    "сантехника": 44,
    "сантехников": 44,
    "свадьбу": 124,
    "секс": 12,
    "сигареты": 92,
    "смс": 50,
    "собак": 54,
    "собаку": 54,
    "соседей": 52,
    "соседа": 52,
    "сочи 2014": 119,
    "сочи": 119,
    "спорт": 10,
    "сталина": 133,
    "студента": 5,
    "студентов": 5,
    "сусанина": 85,
    "такси": 118,
    "таможню": 120,
    "тараканов": 86,
    "таракана": 86,
    "тв": 114,
    "телевидение": 114,
    "телевизор": 114,
    "твиттер": 21,
    "тёщу": 51,
    "трактор": 107,
    "трамвай": 105,
    "троллейбус": 104,
    "фильмы": 115,
    "фсб": 130,
    "футбол": 141,
    "украину": 140,
    "украинцев": 140,
    "украинца": 140,
    "хохла": 140,
    "хохлов": 140,
    "хоккей": 142,
    "хоттабыча": 76,
    "цитаты": 100,
    "чака норриса": 88,
    "чапаева": 1,
    "чебурашку": 63,
    "челябинск": 34,
    "чукчу": 69,
    "шерлока холмса": 6,
    "школу": 25,
    "штирлица": 22,
    "юмор": 99
}

def pretty(s):
    return s.replace("<br>", "\n").strip("\n ")

try:
    with open(SENTFILE) as f:
        bot.anekdot_sent = f.read().strip(" \n").splitlines()
except:
    bot.anekdot_sent = []

@bot.reaction(".*(анек|тем).*(тем|анек).*", personal=True)
def reaction(mes, res, frm, ch):
    themes = list(ANEKDOT_THEMES.keys())
    return "Я знаю анекдоты про: " +  ", ".join(themes)


@bot.reaction("анекдот(.*)", personal=True)
def reaction(mes, res, frm, ch):
    theme = res[1].strip().lower().split()
    theme_num = randint(1, 151)
    mes = ""
    if theme and theme[0] == "про":
        theme = " ".join(theme[1:])
        if theme not in ANEKDOT_THEMES:
            old_theme = theme
            theme = choice(list(ANEKDOT_THEMES.keys()))
            mes = "Про {} я не знаю, вот про {}:\n".format(old_theme, theme)
        theme_num = ANEKDOT_THEMES[theme]

    rss = "http://www.anekdot.ru/rss/tag/{0}.xml".format(theme_num)
    try:
        req = Request(rss)
        req.add_header('User-agent', 'Mozilla/5.0')
        with urlopen(req) as f:
            a = f.read()
        c = parseString(a).documentElement.getElementsByTagName("channel")
        items = c[0].getElementsByTagName("item")[:MAXITEMS]
        got = len(items)
        items = [i for i in items
                 if i.getElementsByTagName("guid")[0].childNodes[0].data
                 not in bot.anekdot_sent]
        if not items and got > MAXITEMS - 2:
            return "На данный момент все актуальные анекдоты я уже постил."
        i = choice(items)
        id = i.getElementsByTagName("guid")[0].childNodes[0].data
        mes += i.getElementsByTagName("description")[0].childNodes[0].data
    except Exception as e:
        bot.log(e)
        return "Что-то сейчас не получается получить анекдоты…"
    else:
        bot.anekdot_sent.append(id)
        with open(SENTFILE, "a") as f:
            f.write(f"\n{id}")
        return pretty(mes)
