# Бот убивает указанного участника >:E
# Примеры: "бот, убей @Looser", "бот, убей Looser"

from go import bot
from random import choice
from constants import MODULES_DIR
import sys
sys.path.insert(0, MODULES_DIR+"/str_resources")
from deaths import DEATHS

@bot.reaction(r"(убей|прикончи) (.*)", personal=True, exclude=r"ты")
def reaction(mes, res, frm, ch):
    if any(mention in mes for mention in ["фурри", "фурриёбов", "фурриебов", "фуррей", "фурей", "фури"]):
        return choice(
            ["С удовольствием! Давайте список.", "*достаёт огнемёт*", "Давно пора", "Я уж думал, вы не попросите."])
    dead = ""
    if "меня" in mes:
        return choice(DEATHS).format(frm)
    if "себя" in mes.lower():
        return "Не дождётесь!"
    if "пушиш" in mes.lower():
        return "Не обижай пушишницу!"
    else:
        mes_with_id = ""
        if "@" in mes:
            mes_with_id = mes.replace("!", "")
        for member in list(bot.get_roster()):
            if mes_with_id and member.mention.replace("!", "") in mes_with_id:
                if member.nick:
                    dead = member.nick
                    break
                dead = member.name
                break
            elif member.nick and member.nick.lower() in mes.lower():
                dead = member.nick
                break
            elif member.name.lower() in mes.lower():
                dead = member.name
                break

    if dead:
        return choice(DEATHS).format(dead)
    else:
        return "Тут нет {}".format(res.group(2))
