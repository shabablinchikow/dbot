# Конвертер валют.
# бот, {количество} {чего} в {что}
# если валюта-результат не указана, предполагается, что это рубли
# бот, 100 гривен в евро
# бот, 100 баксов

from urllib.request import urlopen
from xml.dom.minidom import parseString

from go import bot

CB_URL = "http://www.cbr.ru/scripts/XML_daily.asp"


def to_float(s):
    return float(s.replace(",", "."))


def fancyfloat(n, x=2):
    return f"{n:_.{x}f}".replace("_", " ")


def get_curs():
    try:
        with urlopen(CB_URL) as f:
            data = f.read()
        doc = parseString(data).documentElement  # .getElementsByTagName("ValCurs")
        items = doc.getElementsByTagName("Valute")
        bot.curs = {}
        for i in items:
            bot.curs[i.getElementsByTagName("CharCode")[0].childNodes[0].data] = \
                to_float(i.getElementsByTagName("Value")[0].childNodes[0].data) / \
                to_float(i.getElementsByTagName("Nominal")[0].childNodes[0].data)
        bot.curs["RUB"] = 1
    except:
        pass


@bot.reaction("([0-9][0-9,\.]*) ([A-Z]{3}|₽|рубля|рубль|рублей|руб\.|долларов|доллара|\$|баксов|евро|фунтов|иен|юаней|крон\
    |чешских|китайских|японских|гривен|гривн)(.*)",
    personal=True, ignorecase=False)
def reaction(mes, res, frm, ch):
    get_curs()
    srcamount = float(res.group(1).replace(',', '.'))
    srccur = res.group(2)

    srcdict = {("₽", "руб.", "рублей", "рубль", "рубля"): "RUB",
               ("$", "долларов", "баксов", "доллара", "доллар"): "USD",
               ("евро", "euro"): "EUR",
               ("фунтов", "фунтов стерлингов", "фунт", "фунта", "фунт стерлингов", "фунта стерлингов"): "GBP",
               ("иен", "японских иен"): "JPY",
               ("юаней", "китайских юаней"): "CNY",
               ("чешских крон", "крон чешских"): "CZK",
               ("гривен", "украинских гривен", "гривна", "гривн"): "UAH"}
    for key in srcdict:
        if srccur.lower() in key:
            srccur = srcdict[key]
            break

    if srccur not in bot.curs:
        return f"Не знаю такой валюты {srccur}."
    dst = ""
    try:
        dst = res.group(3)
    except:
        pass
    dst = dst.strip("  ,.?!)(:")
    if dst:
        if dst.startswith("в ") or dst.startswith("to "):
            dst = dst.split(" ", 1)[1].strip()
    dstdict = {"рублях": "RUB",
               "рубли": "RUB",
               "гривнах": "UAH",
               "гривны": "UAH",
               "украинских г": "UAH",
               "украинские г": "UAH",
               "долларах": "USD",
               "доллары": "USD",
               "баксах": "USD",
               "$": "USD",
               "евро": "EUR",
               "фунты": "GBP",
               "фунтах": "GBP",
               "иенах": "JPY",
               "иены": "JPY",
               "юанях": "CNY",
               "юани": "CNY",
               "чешских кронах": "CZK",
               "чешские кроны": "CZK"}
    for key in dstdict:
        if dst.startswith(key):
            dst = dstdict[key]
            break

    if dst[:3] in bot.curs:
        res = (srcamount * bot.curs[srccur]) / bot.curs[dst]
    else:
        dst = "RUB"
        res = srcamount * bot.curs[srccur]
    fancycur = {"RUB": "₽",
                "USD": "$",
                "EUR": "€",
                "GBP": "£",
                "JPY": "¥",
                "UAH": "₴"}
    if srccur in fancycur:
        srccur = fancycur[srccur]
    if dst in fancycur:
        dst = fancycur[dst]
    return f"{fancyfloat(srcamount)} {srccur} = {fancyfloat(res)} {dst}"


get_curs()
