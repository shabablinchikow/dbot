# Переводчик. Если просят перевести одно слово, используются встроенные словари.
# Если много, то яндекс-переводчик.

import gzip

from go import bot
from urllib.request import urlopen
from urllib.parse import quote
from xml.dom.minidom import parseString

BASE= "https://translate.yandex.net/api/v1.5/tr/translate"
API_KEY = "trnsl.1.1.20170112T133155Z.\
f66e6222020d60c9.06c2c7a3b07db162daaeb771f0b0d6f2a507df31"

langs = {
    'az': 'азербайджанского',
    'mt':  'мальтийского',
    'sq': 'албанского',
    'mk': 'македонского',
    'am': 'амхарского',
    'mi': 'маори',
    'en': 'английского',
    'mr': 'маратхи',
    'ar': 'арабского',
    'mhr': 'марийского',
    'hy': 'армянского',
    'mn': 'монгольского',
    'af': 'африкаанс',
    'de': 'немецкого',
    'eu': 'баскского',
    'ne': 'непальского',
    'ba': 'башкирского',
    'no': 'норвежского',
    'be': 'белорусского',
    'pa': 'панджаби',
    'bn': 'бенгальского',
    'pap': 'папьяменто',
    'bg': 'болгарского',
    'fa': 'персидского',
    'bs': 'боснийского',
    'pl': 'польского',
    'cy': 'валлийского',
    'pt': 'португальского',
    'hu': 'венгерского',
    'ro': 'румынского',
    'vi': 'вьетнамского',
    'ru': 'русского',
    'ht': 'Haitian (Creole)',
    'ceb': 'себуанского',
    'gl': 'галисийского',
    'sr': 'сербского',
    'nl': 'нидерландского',
    'si': 'сингальского',
    'mrj': 'горномарийского',
    'sk': 'словацкого',
    'el': 'греческого',
    'sl': 'словенского',
    'ka': 'грузинского',
    'sw': 'суахили',
    'gu': 'гуджарати',
    'su': 'сунданского',
    'da': 'датского',
    'tg': 'таджикского',
    'he': 'иврита',
    'th': 'тайского',
    'yi': 'идиша',
    'tl': 'тагальского',
    'id': 'indonesian',
    'ta': 'тамильского',
    'ga': 'ирландского',
    'tt': 'татарского',
    'it': 'итальянского',
    'te': 'телугу',
    'is': 'исландского',
    'tr': 'турецкого',
    'es': 'испанского',
    'udm': 'удмуртского',
    'kk': 'казахского',
    'uz': 'узбекского',
    'kn': 'каннада',
    'uk': 'украинского',
    'ca': 'каталанского',
    'ur': 'урду',
    'ky': 'киргизского',
    'fi': 'финнского',
    'zh': 'китайского',
    'fr': 'французского',
    'ko': 'корейского',
    'hi': 'хинди',
    'xh': 'коса',
    'hr': 'хорватского',
    'la': 'латинского',
    'cs': 'чешского',
    'lv': 'латвийского',
    'sv': 'шведского',
    'lt': 'литовского',
    'gd': 'шотландского',
    'lb': 'люксембуржского',
    'et': 'эстонского',
    'mg': 'малагасийского',
    'eo': 'эсперанто',
    'ms': 'малайского',
    'jv': 'яванского',
    'ml': 'малаялам',
    'ja': 'японского'
}


def translate(word, dictfile):
    with gzip.open(dictfile) as f:
        lines = f.readlines()
    t = False
    transl = ""
    for line in lines:
        line = line.decode()
        if line.strip("\n") == word:
            t = True
        elif t:
            if line.startswith(" "):
                transl += line
            elif line == "\n":
                pass
            else:
                return transl.strip(" \n")
    return transl


def yandex_translate(text):
    url = f"{BASE}?key={API_KEY}&text={text}&lang=ru"
    with urlopen(url) as f:
        data = f.read().decode('utf-8')
    return parseString(data).documentElement.getElementsByTagName(
        "text")[0].childNodes[0].data


@bot.reaction("(?:переведи|как переводится) (.*)", personal=True)
def reaction(mes, res, frm, ch):
    if "вошел" in mes or "вышел" in mes or "забанен" in mes or "вошёл" in mes:
        return ""
    text = res.group(1).strip()
    if not text:
        return
    t = ''
    if len(text.split()) == 1:
        t = translate(text, bot.datadir+"/mueller-base.dict.dz")
        if t:
            return f'{text} переводится с английского как:\n{t}'
        t = translate(text, bot.datadir+"/smirnitsky.dict.dz")
        if t:
            return f'{text} переводится на английский как:\n{t}'

    return yandex_translate(quote(text))
