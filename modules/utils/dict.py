# Толковые словари и вики.
# бот, что такое программирование

import gzip
from urllib.request import urlopen
from urllib.parse import quote
import json

from go import bot

DICT_FILE_RU_EFR = bot.datadir+"/efr.dict.dz"
WIKI_API = "https://ru.wikipedia.org/w/api.php"
queries = (
    "action=query",
    "formatversion=2",
    "redirects",
    "explaintext",
    "format=json",
    "exsectionformat=plain",
    "&exsentences=4",
)


with gzip.open(DICT_FILE_RU_EFR) as f:
    d = f.read().decode('utf-8').split("\n\n")
bot.dict_efr = {}
for s in d:
    lines = s.splitlines()
    word = lines[0]
    df = "\n".join(line.strip() for line in lines[1:] if line.startswith(" "))
    bot.dict_efr[word] = df.strip(" \n")


def get_from_wiki(s):
    title = quote(s)
    url = f"{WIKI_API}?{'&'.join(queries)}&titles={title}&prop=extracts|pageterms"
    with urlopen(url) as f:
        data = f.read().decode('utf-8')
    j = json.loads(data)
    return j["query"]["pages"][0]["extract"]


@bot.reaction("вики (.*)", personal=True, ignorecase=False)
def reaction(mes, res, frm, ch):
    phrase = res.group(1)
    try:
        print(phrase)
        desc = get_from_wiki(phrase)
    except:
        return f"На википедии нет статьи «{phrase}»"
    else:
        return desc


@bot.reaction("(кто такой|что такое) (.*)", personal=True, ignorecase=False)
def reaction(mes, res, frm, ch):
    phrase = res.group(2)
    if phrase.strip():
        word = phrase.strip().split()[0].strip(",.?!«»<>\"'  ").lower()
    if word in bot.dict_efr:
        return f"Согласно словарю Ефремовой, {word} это:\n{bot.dict_efr[word]}"
    phrase = phrase.strip(",.?!<> \n").replace(" ", "_")
    try:
        print(phrase)
        desc = get_from_wiki(phrase)
    except:
        pass
    else:
        return desc
    if word.lower() != phrase.lower():
        dop = f" (и фразы {phrase} в википедии)"
    else:
        dop = ""
    return f"Слова «{word}» в словаре Ефремовой{dop} нет ☹"
