# Бот рассказывает новость с новостного сайта.
# бот, новости
# бот, что в мире происходит?


from html import unescape
from random import choice
from urllib.request import Request, urlopen
from xml.dom.minidom import parseString


from go import bot

NEWSRSS = "http://news.yandex.ru/index.rss"
MAXITEMS = 20
SENTFILE = bot.datadir + "/newssent"

try:
    with open(SENTFILE) as f:
        bot.newssent = f.read().strip(" \n").splitlines()
except:
    bot.newssent = []


@bot.reaction("(новости?|в мире)", personal=True)
def reaction(mes, res, frm, ch):
    try:
        req = Request(NEWSRSS)
        req.add_header('User-agent', 'Mozilla/5.0')
        with urlopen(req) as f:
            a = f.read()
        c = parseString(a).documentElement.getElementsByTagName("channel")
        items = c[0].getElementsByTagName("item")[:MAXITEMS]
        got = len(items)
        items = [i for i in items
                 if i.getElementsByTagName("guid")[0].childNodes[0].data
                 not in bot.newssent]
        if not items and got > MAXITEMS - 2:
            return "На данный момент все актуальные новости я уже постил."
        i = choice(items)
        id = i.getElementsByTagName("guid")[0].childNodes[0].data
        mes = i.getElementsByTagName("description")[0].childNodes[0].data
        title = i.getElementsByTagName("title")[0].childNodes[0].data
        mes = title + "\n" + unescape(mes)
    except Exception as e:
        bot.log(e)
        return "Что-то сейчас не получается получить новости…"
    else:
        bot.newssent.append(id)
        with open(SENTFILE, "a") as f:
            f.write(f"\n{id}")
        return mes.strip("\n ")
