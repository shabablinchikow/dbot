from go import bot
from datetime import datetime


@bot.reaction()
def reaction(mes, res, frm, ch):
    bot.log(f"{datetime.now()}: {frm}: {mes}")
