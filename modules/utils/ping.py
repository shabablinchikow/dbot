from go import bot


@bot.reaction(r"^(пинг|ping|gbyu|зштп)\b", personal=False, ignorecase=True)
def ping(mes, res, frm, ch):
    if res.group(1).lower() == "ping":
        return "pong"
    elif res.group(1).lower() == "gbyu":
        return "понг, бНОПНЯ клятая"
    elif res.group(1).lower() == "зштп":
        return "pong, you russian swine!"
    return "понг"

@bot.reaction(r"(пинг|ping|gbyu)\b", personal=True, ignorecase=True)
def ping1(mes, res, frm, ch):
    return f"{frm}, понг"
