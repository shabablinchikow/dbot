from urllib.request import urlopen
from urllib.parse import quote
from xml.dom.minidom import parseString

from helpers import get_jsonparsed_data

from go import bot

YAN_BASE= "https://translate.yandex.net/api/v1.5/tr/translate"
YAN_API_KEY = "trnsl.1.1.20170112T133155Z.\
f66e6222020d60c9.06c2c7a3b07db162daaeb771f0b0d6f2a507df31"

OW_BASE = "https://api.openweathermap.org/data/2.5/weather"
OW_API_KEY = "523bba2c9ce2aacb588bb86a6dbd7bc2"
OW_LANG = "ru"

def get_cityname(city):
    city = quote(city)
    url = f"{YAN_BASE}?key={YAN_API_KEY}&text={city}&lang=en"
    with urlopen(url) as f:
        engcity = f.read().decode("utf-8")
    return parseString(engcity).documentElement.getElementsByTagName(
        "text")[0].childNodes[0].data

def get_weather(engcity):
    url = f"{OW_BASE}?q={engcity}&appid={OW_API_KEY}&lang={OW_LANG}"
    weather = get_jsonparsed_data(url)
    main = weather.get('main')
    temp = main.get('temp')
    temp = str(int(temp) - 273)
    icon =  weather.get('weather')[0].get('icon')

    conditions = {
        '01d': ':sunny:',
        '02d': ':white_sun_small_cloud: ',
        '03d': ':cloud:',
        '04d': ':cloud:',
        '09d': ':cloud_rain:',
        '10d': ':white_sun_rain_cloud:',
        '11d': ':thunder_cloud_rain:',
        '13d': ':cloud_snow:',
        '50d': ':cloud:',
        '01n': ':full_moon:',
        '02n': ':cloud:',
        '03n': ':cloud:',
        '04n': ':cloud:',
        '09n': ':cloud_rain:',
        '10n': ':cloud_rain:',
        '11n': ':thunder_cloud_rain:',
        '13n': ':cloud_snow:',
        '50n': ':cloud:'
        }
    for k, v in conditions.items():
        if icon == k:
            icon = v
            break

    return temp + "°C " + icon


@bot.reaction(r"(погода|погоду|погодой|погоде) (в |по |на )?(.*)", ignorecase=False, personal=True)
def reaction(mes, res, frm, ch):

    city = res.group(3).strip()
    engcity = get_cityname(city.encode('utf-8'))
    weather = get_weather(engcity)
    return  weather