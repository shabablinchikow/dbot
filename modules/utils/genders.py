#remember female users
from go import bot
from helpers import save_obj, load_obj

@bot.reaction(r"я.*(девушка|девочка|женщина|баба|жеского|дама|мадам|леди|дева|тян)", personal=True, ignorecase=True)
def reaction(mes, res, frm, ch):
    x = load_obj('females')
    uid = frm.strip("<!@>")
    if not x:
        x = []
    if '?' in mes:
        if uid in x:
            return f"Да, {frm} точно {res.group(1)}"
        else:
            return f"Нет, {frm} не {res.group(1)}"
    if any(w in mes for w in ["не", "забудь"]) and uid in x:
        x.remove(uid)
        save_obj(x, 'females')
        return f"Хорошо, я запомню, что {frm} не {res.group(1)}!"
    elif uid not in x:
        x.append(uid)
    save_obj(x, 'females')
    return f"Хорошо, я запомню, что {frm} {res.group(1)}!"
