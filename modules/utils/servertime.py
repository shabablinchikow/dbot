# Get server time.

from go import bot
from datetime import datetime

@bot.reaction(r"\b(москве|московское|мск|серверное)\b", personal=True, exclude="погода")
def reaction(mes, res, frm, ch):
    if "время" in mes:
        return datetime.now().strftime('%H:%M')
    else:
        return "Время по москве: " + datetime.now().strftime('%H:%M')
