# Калькулятор. Работает с простыми выражениями, реакция срабатывает на ключевые слова
# считай|посчитай|реши|сколько будет

from multiprocessing import Pool
from go import bot
from specialmessages import LowPriorityMessage


def calculate(a):
    try:
        res = eval(a)
    except:
        res = None
    return res


# WARNING: `calculate` function uses eval, so check regexp twice!
@bot.reaction("(считай|посчитай|реши|сколько будет) ([0-9 \*\+/\)\(\.,\^-]+)", personal=True)
def reaction(mes, res, frm, ch):
    ask = res.group(2).replace(",", ".").replace("^", "**")
    with Pool() as p:
        pp = p.apply_async(calculate, [ask])
        try:
            r = pp.get(timeout=2)
            done = True
        except:
            r = None
            done = False
    if not done:
        return "Это считать слишком долго"
    if r is None:
        return "Я не могу это посчитать"
#    return f"`{ask} = {r:<10_.15g}`".replace("_", " ")
    res = f"{r:<10_.15g}".replace("_", " ").replace("*", "\*")

    if ask.strip() == res.strip():
        return
    return LowPriorityMessage(f"{ask} = {res}")
