# бот, брось кубик
# бот, кинь монетку
# d6, d20, d100, d222
# % посредственно (см. систему FUDGE)

from random import randint, choice, seed
from specialmessages import LowPriorityMessage
from go import bot

desc = "бросать кубик и монетку"


@bot.reaction("(брос|кин)ь.*(кубик|[дd]6)", personal=True)
def reaction(mes, res, frm, ch):
    return choice("⚀⚁⚂⚃⚄⚅")


@bot.reaction(r"(монетк?у)", personal=True)
def reaction(mes, res, frm, ch):
    if not any(w in mes for w in ["брось", "кинь", "бросай", "кидай"]):
        return LowPriorityMessage("Я не подаю милостыню")
    else:
        rp = randint(0, 1000)
        if not rp:
            return "РЕБРО!!!111 Прикиньте! РЕБРО!!!"
        if rp % 2:
            return "Орёл!"
        else:
            return "Решка!"


@bot.reaction(r"^d([0-9]*)\b(.*)", ignorecase=False)
def dice(mes, res, frm, ch):
    dice = int(res.group(1))
    if dice > 300:
        return "А не дохуя?"
    if dice == 1:
        return "Серьёзно? Чего ты ожидаешь?"
    additional = res.group(2).strip()
    if additional:
        additional = f" ({additional})"
    return f"{frm} кидает d{dice}{additional}, выпадает {randint(1, dice)}"


@bot.reaction(r"^%(\x20)?(.*)", ignorecase=False, personal=False)
def dice(mes, res, frm, ch):
    results = {-3: "так ужасно, что хуже некуда",
               -2: "ужасно---",
               -1: "ужасно--",
               0: "ужасно-",
               1: "ужасно",
               2: "плохо",
               3: "посредственно",
               4: "нормально",
               5: "хорошо",
               6: "отлично",
               7: "превосходно",
               8: "легендарно",
               9: "легендарно+",
               10: "легендарно++",
               11: "легендарно+++",
               12: "ТАК ЛЕГЕНДАРНО, ЧТО ПОЗАВИДУЕТ ДАЖЕ НЕБО, ДАЖЕ АЛЛАХ!"
               }
    n = 2
    inp = mes[2::]
    if inp.startswith("ужасно"):
        n = 1
    elif inp.startswith("плохо"):
        n = 2
    elif inp.startswith("посредственно"):
        n = 3
    elif inp.startswith("нормально"):
        n = 4
    elif inp.startswith("хорошо"):
        n = 5
    elif inp.startswith("отлично"):
        n = 6
    elif inp.startswith("превосходно"):
        n = 7
    elif inp.startswith("легендарно"):
        n = 8
    else:
        return ""
    rolls = ''
    for i in range(0, 4):
        roll = choice([-1, 0, 1])
        if roll < 0:
            rolls += "-"
        elif roll == 0:
            rolls += "="
        if roll > 0:
            rolls += "+"
        n += roll
    rollout = results.get(n)
    return f"{frm} бросает 4dF ({rolls}) от {inp}. Результат: {rollout}"


seed()
