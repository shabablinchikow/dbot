from go import bot, reload_modules
from helpers import gen_text_model
from specialmessages import LowPriorityMessage
from constants import OWNER_ID


@bot.reaction("!update")
def reaction(mes, res, frm, ch):
    if not OWNER_ID in frm:
        return "У вас нет прав :stuck_out_tongue: "
    else:
        gen_text_model()
        return "Текстовая модель обновлена!"


@bot.reaction("!reload")
def reaction(mes, res, frm, ch):
    if not OWNER_ID in frm:
        return "Что-что?"
    reload_modules()
    bot.log("Modules reload was explicitly called by " + frm)
    return "Слушаюсь!"
