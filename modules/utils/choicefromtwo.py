# Бот выбирает один из двух вариантов в запросе "бот, быть или не быть"

from random import choice, seed, randint

from specialmessages import LowPriorityMessage
from go import bot
from re import sub


@bot.reaction(r"(.+) или (.+)", personal=True, ignorecase=False)
def reaction(mes, res, frm, ch):
    flag=0
    if not randint(0, 100):
        return "или"
    first = res.group(1)
    first = first.split(" ", 1)[1].strip("  !?,.")

    if first.startswith("я"):
        first = sub(r"^я\b", "ты", first)
        flag=1
    if (first.startswith("ты") and (flag==0)):
        first = sub(r"^ты\b", "я", first)

    if res.group(2).startswith("я"):
        second = sub(r"^я\b", "ты", res.group(2).strip("  !?,."))
        flag=1
    if (res.group(2).startswith("ты") and (flag==0)):
        second = sub(r"^ты\b", "я", res.group(2).strip("  !?,."))
    else:
        second = res.group(2).strip("  !?,.")
    return LowPriorityMessage(choice((first, second)))


seed()
