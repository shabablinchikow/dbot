# Модуль, показывающий музыку из вашего статуса вконтакте. Подробнее смотри !vk help ниже.

import json
import urllib.request

from go import bot
from helpers import save_obj, load_obj, get_jsonparsed_data
from constants import VK_TOKEN


@bot.reaction(r"^!(?:вк|vk)\b(.*)", personal=False)
def reaction(mes, res, frm, ch):
    if '!' in frm:
        frm = frm.replace("!", "")
    VKIDS = load_obj("vkids")
    if ch == "quiz":
        return
    if res.group(1) == " help":
        return "Чтобы я показал всем, какую аудиозапись ты слушаешь вконтакте, нужно добавить свой id и включить \
        трансляцию музыки в статус!\n\n!vk или !вк: вызывать название текущего трека\n!vk add <id>: добавить свой id,\
         можно отправлять мне в личку. Он должен состоять из цифр. Увидеть его можно, например, в ссылке на ваши аудио\
          (vk.com/audios<id>). Чтобы обновить свой id, используйте эту же команду.\n!vk deleteme: удаляет ваш айди\
           из базы."
    if "add" in res.group(1):
        id = res.group(1).strip("add ")
        if len(id) < 1 or len(id) > 9:
            return "Похоже, ваш id слишком длинный или слишком короткий. Мне очень жаль!"
        try:
            id_check = int(id)
        except ValueError:
            return "Что-то пошло не так. Убедись, что id состоит только из цифр и не превышает девяти знаков.\
             Увидеть айди можно, например, в ссылке на ваши аудио (vk.com/audios<id>)."
        if id_check <= 0:
            return "С этим айди явно что-то не так. Попробуй положительную величину!"
        if not VKIDS:
            VKIDS = {}
        VKIDS.update({frm: id})
        save_obj(VKIDS, 'vkids')
        print(VKIDS.get(frm))
        return "id успешно добавлен!"
    if res.group(1) == " deleteme":
        if not VKIDS:
            return "Я не нашёл файла vkids.pkl! Похоже, я не знаю ни одного id. См. !vk help"
        if frm not in VKIDS.keys():
            return "А у меня и нет твоего айди..."
        else:
            print(frm)
            del VKIDS[frm]
            save_obj(VKIDS, 'vkids')
            return "id успешно удалён!"
    if not VKIDS:
        return "Я не нашёл файла vkids.pkl! Похоже, я не знаю ни одного id. См. !vk help"
    if frm not in VKIDS.keys():
        return "Прости, но у меня нет твоего id вконтакте. Чтобы добавить его, напиши !vk add <id> \
        (например, !vk add 12345). ID должен состоять из цифр.\nДля справки напиши !vk help."
    vkid = VKIDS.get(frm)
    vkid = str(vkid)
    vkurl = f"https://api.vk.com/api.php?oauth=1&access_token={VK_TOKEN}&method=users.get&user_id={vkid}&fields=status&v=5.52"
    info = get_jsonparsed_data(vkurl)
    info = info.get('response')[0]
    id = info.get('id')
    name = info.get('first_name') + " " + info.get('last_name')
    audio = info.get('status_audio')
    if not audio:
        return f"Похоже, что {frm} ничего не слушает вконтакте на данный момент, либо не транслирует музыку в статус."
    nowpl = f"{frm} слушает песню «{audio.get('title')}» в исполнении {audio.get('artist')}"
    save_obj(VKIDS, 'vkids')
    return nowpl
