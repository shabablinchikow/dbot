from random import choice

from helpers import make_sent


class Dialog:
    def __init__(self, id):
        self.id = id
        self.stage = 0
        self.last_stage = 0
        self.dead = False
        self.init_message = None
        self.end_message = None

    def process(self, message):
        pass

    def next(self):
        self.stage += 1
        if self.stage == self.last_stage:
            self.dead = True

    def __str__(self):
        return f"Диалог с id {self.id} на фазе {self.stage}, init = {self.init_message}, end = {self.end_message}"


class TestDialog(Dialog):
    def __init__(self, id):
        Dialog.__init__(self, id)
        self.init_message = "Начинаем диалог!"
        self.end_message = "Здорово поболтали!"
        self.last_stage = 3

    def process(self, message):
        if not self.stage:
            self.next()
            return "Погоди, я думаю..."
        if self.stage == 1 and "раз" in message:
            self.next()
            return "Два"
        if self.stage == 2 and "два" in message:
            self.next()
            return "Три"
        if self.stage == 3 and "три" in message:
            self.next()


class TalkDialog(Dialog):
    def __init__(self, id):
        Dialog.__init__(self, id)
        self.init_message = choice(["Давай! О чём?", "Почему бы и нет.", "Ну давай."])
        self.end_message = "Здорово поболтали!"
        self.last_stage = 3

    def process(self, message):
        self.next()
        return make_sent()
    