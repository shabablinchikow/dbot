import datetime
import re
import traceback
from random import randint

from constants import *
from helpers import make_sent, process_personal, parse_personal, log_miss, load_obj, save_obj
from specialmessages import BreakingMessage, LowPriorityMessage, FileToSend
from discord import User

now = datetime.datetime.now


class BaseBot():
    """Represents bot

    Attributes
    -----------

    reactions :
        a list of functions added from modules

    datadir : string
        path to data

    personals :
        list of strings which represent bot's names

    client :
        link to discord.Client object

    skills : TODO
        list of string describing what bot can do based on active modules
    """

    def __init__(self, client):
        self.reactions = []
        self.datadir = DATA_DIR
        self.personals = PERSONALS
        self.client = client
        self.games = {}
        self.dialogs = {}

    def log(self, mes):
        print(mes)

    def set_personals(self, *args):
        self.personals = args

    def add_reaction(self, func):
        self.reactions.append(func)
        return func

    def add_dialog(self, dialog):
        if dialog.id in self.dialogs.keys():
            return False
        print(f"DEBUG: был добавлен {dialog}")
        self.dialogs[dialog.id] = dialog
        return True

    def remove_dialog(self, dialog):
        if dialog.id not in self.dialogs.keys():
            return False
        print(f"DEBUG: был удалён {dialog}")
        self.dialogs.pop(dialog.id)
        return True

    def add_game(self, game):
        if type(game).__name__ in self.games.keys():
            return False
        self.games[type(game).__name__] = game
        return True

    def remove_game(self, game):
        if type(game).__name__ not in self.games.keys():
            return False
        self.games.pop(type(game).__name__)
        return True



    # get list of members from discord server
    def get_roster(self):
        return (list)(self.client.get_all_members())

    def user_has_role(self, userid, rolename):
        for member in self.get_roster():
            if member.id == userid:
                for role in member.roles:
                    if role.name == rolename:
                        return True
        return False

    def is_message_personal(self, s):
        s = s.strip(" .!?;):(").lower()
        if any(s.startswith(p.lower()) for p in self.personals) or \
                any(s.endswith(f", {p.lower()}") for p in self.personals):
            return True
        else:
            return False

    def onmessage(self, msg):
        answers = []
        for r in self.reactions:
            try:
                answer = r(msg)
            except Exception:  # as e
                self.log(msg.content)
                traceback.print_exc()
            else:
                if answer:
                    if type(answer) == str:
                        answers.append(answer)
                    elif type(answer) == BreakingMessage:
                        answers.append(answer)
                        break
                    elif type(answer) == LowPriorityMessage and not answers:
                        answers.append(answer)
                    elif type(answer) == FileToSend:
                        return answer
                    else:
                        pass  # TODO: breaking answers, desire answers etc
        without_low = [a for a in answers if type(a) != LowPriorityMessage]
        if without_low:
            answers = without_low

        if not len(answers):
            content = msg.content
            if msg.channel.id in TEXT_MODEL_ID:
                phr = load_obj('phr')
                if not phr:
                    phr = []
                phr_cont = content
                if self.is_message_personal(content):
                    phr_cont = content.replace(parse_personal(content), "")
                if 2 < len(phr_cont) < 300 and not any(ex in phr_cont.lower() for ex in["@", "http", "www"]):
                    phr.append(phr_cont)
                if len(phr) > 50000:
                    del phr[randint(0, 5000)]
                save_obj(phr, 'phr')

            if self.is_message_personal(content):
                # log a reaction that was not found
                log_miss(content)

                # check if bot has something to say
                res = process_personal(content, parse_personal(content))
                if res:
                    self.log("{0}: Этот бот: {1}".format(now(), res))
                    return res

                # if bot has nothing to say, improvise!
                res = make_sent()
                self.log("{0}: Этот бот: {1}".format(now(), res))
                return res

        self.log("{0}: Этот бот: {1}".format(now(), "\n".join(set(answers))))
        return "\n".join(set(answers))

    def reaction(self, regex=".*", ignorecase=True, personal=False, exclude=""):
        def wrapper(f):
            def pr(msg):
                mes = msg.content
                frm = msg.author.mention
                ch = msg.channel.name
                if ignorecase:
                    mi = mes.lower()
                else:
                    mi = mes
                if personal and not self.is_message_personal(mes):
                    return
                if exclude != "":
                    ex = re.search(exclude, mi)
                    if ex:
                        return
                res = re.search(regex, mi)
                if not res:
                    return
                return f(mes, res, frm, ch)

            self.add_reaction(pr)

        #            return pr
        return wrapper

    def add_buffer(self, mes):
        self.message_buffer.append(mes)

    def get_main_person(self):
        return self.personals[0]

# bot = BaseBot()
