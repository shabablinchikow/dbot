import gzip
import pickle
import re
from random import choice, randint
from urllib.request import Request, urlopen
from xml.dom.minidom import parseString
import json

import markovify
from constants import DATA_DIR, PERSONALS, ANSWERS, REACTTO, SWEAR_ANSWERS


def dict_replace(m, d):
    mes = m
    for v in d.keys():
        mes = mes.replace(v, d[v])
    return mes


def dehtml(m):
    return dict_replace(m, {
        "<br>": "\n",
        "<br />": "\n",
        "&quot;": '"',
        "&gt;": ">",
        "&lt;": "<",
        "&amp;": "&"})
    return m


def one_from_rss(url, maxitem=50):
    try:
        req = Request(url)
        req.add_header('User-agent', 'Mozilla/5.0')
        with urlopen(req) as f:
            a = f.read()
        b = parseString(a).documentElement
        c = b.getElementsByTagName("channel")
        i = choice(c[0].getElementsByTagName("item")[:maxitem])
        d = i.getElementsByTagName("description")[0].childNodes[0].data
        mes = dict_replace(d, {
            "<br>": "\n",
            "<br />": "\n",
            "&quot;": '"',
            "&gt;": ">",
            "&lt;": "<",
            "&amp;": "&"})
        a = b = c = i = d = None
        return mes.strip("\n ")
    except:
        return ''


def readefr():
    with gzip.open(DICT_FILE_RUEFR) as f:
        d = f.read().decode('utf-8').split("\n\n")
    dct = {}
    for s in d:
        lines = s.splitlines()
        word = lines[0]
        df = ""
        for line in lines[1:]:
            if line.startswith(" "):
                df += "\n" + line.strip()
            else:
                print("aaaa")
        dct[word] = df.strip(" \n")
    return (dct)


def upperfirst(x):
    return x[0].upper() + x[1:]


# get either author's name or his name and local nickname
def mes_author(msg):
    author = msg.author.name
    if msg.author.nick:
        author += " aka " + msg.author.nick
    return author


# save .pkl
def save_obj(obj, name):
    with open(DATA_DIR + '/' + name + '.pkl', 'wb') as f:
        pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)


# load .pkl
def load_obj(name):
    try:
        with open(DATA_DIR + '/' + name + '.pkl', 'rb') as f:
            return pickle.load(f)
    except FileNotFoundError:
        #        return f"Я не знаю, что ответить :worried:. Проверьте наличие файла {name}.pkl в директории data."
        return ""


# decline words with given number
def decl(x, word):
    # x = 33
    # word = ["год", "года", "лет"]
    inumber = x % 100
    if inumber >= 11 and inumber <= 19:
        return word[2]
    else:
        iinumber = inumber % 10
        if iinumber == 1:
            return word[0]
        elif iinumber == 2 or iinumber == 3 or iinumber == 4:
            return word[1]
        else:
            return word[2]


# generate a text model using markovify
def gen_text_model():
    #    class POSifiedText(markovify.Text):
    #        def word_split(self, sentence):
    #            return ["::".join((word.orth_, word.pos_)) for word in nlp(sentence)]

    #        def word_join(self, words):
    #            sentence = " ".join(word.split("::")[0] for word in words)
    #            return sentence

    x = load_obj('phr')
    if not x:
        return
    str = ''
    for obj in x:
        if not re.match(r'[.?!…]$', obj):
            if ' ' in obj:
                obj = obj + '.'
                obj = obj.lower()
                obj = obj.capitalize()
        obj = re.sub(r'[.]{4,}', '.', obj)
        obj = re.sub(r'!\.', '!', obj)
        obj = re.sub(r'\?\.', '?', obj)
        obj = re.sub(r'…\.', '…', obj)
        obj = re.sub(r'<@.*>', '', obj)
        obj = obj.lstrip()

        if not any(ex in obj.lower() for ex in ['www', 'http', '.com', '.ru', '.org', '@']) and len(obj) > 2:
            str += '\n' + obj
    text_model = markovify.Text(str, state_size=1)
    save_obj(text_model, "text_model")


# generate a sentence using markovify
def make_sent():
    text_model = load_obj("text_model")
    if not text_model:
        return ""
    if choice([1, 2, 3, 4]) == 1:
        return text_model.make_short_sentence(300, max_overlap_ratio=90,
                                              max_overlap_total=100).lower().capitalize() + ' ' + text_model.make_short_sentence(
            300, max_overlap_ratio=10, max_overlap_total=5).capitalize()
    return text_model.make_short_sentence(500, max_overlap_ratio=50, max_overlap_total=100).lower().capitalize()

def make_sent_with_length(length):
    text_model = load_obj("text_model")
    if not text_model:
        return ""
    while True:
        try:
            x = text_model.make_short_sentence(length, max_overlap_ratio=50, max_overlap_total=100).lower().capitalize()
            if x:
                return x
        except:
            continue




def process_personal(mes, mention):
    # if bot's name is mentioned along with question mark
    if any(pers + "?" == mes.lower() for pers in PERSONALS):
        return choice(ANSWERS)

    for qst in REACTTO:
        # check if this is a short question. If so, give a short answer
        if qst in mes.lower() and '?' in mes and len(mes) - len(mention) - len(qst) < 5:
            if choice(range(0, 6)):
                return choice(ANSWERS)
            else:
                return choice(SWEAR_ANSWERS)

    # if bot is mentioned but nothing was asked
    if any(pers == mes.strip().lower() for pers in PERSONALS):
        return "Что?"


def parse_personal(msg):
    for p in PERSONALS:
        if p in msg.lower():
            return p
    return ""


# log questions that bot has no answer to
def log_miss(msg):
    with open(DATA_DIR + "/misses.txt", "a") as f:
        f.write(msg + '\n')

def get_jsonparsed_data(url):
    """
    Receive the content of ``url``, parse it as JSON and return the object.

    Parameters
    ----------
    url : str

    Returns
    -------
    dict
    """
    response = urlopen(url)
    data = response.read().decode("utf-8")
    return json.loads(data)

